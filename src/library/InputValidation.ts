/*-----------------------------------------------------------------------------
 * @package:    React Project Template
 * @author:     Richard Winters
 * @copyright:  2022 Richard B Winters
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/


 // INCLUDES


 // DEFINES
export type InputValidationType = {
    valid: boolean,
    exists: boolean;
    required: boolean;
    label: string;
    input: string;
    validation: ValidationType;
};

export type ValidationType = {
    class: string;
    message: string;
};

