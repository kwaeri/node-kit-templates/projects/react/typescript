/*-----------------------------------------------------------------------------
 * @package:    MyNewTypescriptReactProject
 * @author:     Richard Winters
 * @copyright:  2022 Richard B Winters
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/


// INCLUDES
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './redux/store/index';
import { App } from './containers/App';


// DEFINES


// Render the application:
ReactDOM.render
(
    // Render our application's root component, wrapped with the react-redux provider component:
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById( "kwaeri-app" )
);

