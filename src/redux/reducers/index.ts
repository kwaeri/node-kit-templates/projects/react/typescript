/*-----------------------------------------------------------------------------
 * @package:    React Project Template
 * @author:     Richard Winters
 * @copyright:  2022 Richard B Winters
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/


// INCLUDES
import { combineReducers } from 'redux';
import account from './account';


// DEFINES
const rootReducer = combineReducers({
    account
});


// Export it:
export default rootReducer;

