/*-----------------------------------------------------------------------------
 * @package:    React Project Template
 * @author:     Richard Winters
 * @copyright:  2022 Richard B Winters
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/


// INCLUDES


// DEFINES


// Constants:

/* The loading state, an action for displaying a loader and wiping errors: */
export const FETCH_FILMS_BEGIN = "FETCH_FILMS_BEGIN";

/* The success state, an action for displaying API items: */
export const FETCH_FILMS_SUCCESS = "FETCH_FILMS_SUCCESS";

/* The failure state, an action for displaying errors: */
export const FETCH_FILMS_ERROR = "FETCH_FILMS_ERROR";

