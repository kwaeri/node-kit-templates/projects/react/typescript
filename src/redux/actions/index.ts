/*-----------------------------------------------------------------------------
 * @package:    React Project Template
 * @author:     Richard Winters
 * @copyright:  2022 Richard B Winters
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/


// INCLUDES
import fetch from 'cross-fetch';
import {
    FETCH_ITEMS_BEGIN,
    FETCH_ITEMS_SUCCESS,
    FETCH_ITEMS_ERROR
} from '../constants/action-types';


// DEFINES


/* Dispatch Actions for fetching a list of items from an API: */
export const fetchItemsBegin = () => ( { type: FETCH_ITEMS_BEGIN } );

export const fetchItemsSuccess = ( items: any ) => ( { type: FETCH_ITEMS_SUCCESS, payload: { items } } );

export const fetchItemsError = ( error: any ) => ( { type: FETCH_ITEMS_ERROR, payload: { error } } );


/**
 * Method to get a list of items from an API via `fetch`
 *
 * @param { void }
 *
 * @return { Promise<any> }
 *
 * @since 1.0.0
 */
export function fetchItems()
{
    console.log( 'Fetching items!' );
    return ( dispatch: any ) => {

        dispatch( fetchItemsBegin() );
        return fetch( "https://www.snagfilms.com/apis/films.json?limit=10" )
        .then( handleErrors )
        .then( response => response.json() )
        .then
        (
            ( json ) =>
            {
                console.log( 'Dispatching success action: ' );
                console.log( json.items );

                dispatch( fetchItemsSuccess( json.items ) );

                return json.items;
            }
        )
        .catch( ( error ) => dispatch( fetchItemsError( error ) ) );
    };
}


/**
 * A method to handle HTTP Errors
 *
 * @param { any } response The fetch response
 *
 * @return { any } Returns the response if no errors, else it throws the error
 *
 * @since 1.0.0
 */
function handleErrors( response: any )
{
    if( !response.ok )
    {
        throw Error( response.statusText );
    }


    return response;
}

