/*-----------------------------------------------------------------------------
 * @package:    React Project Template
 * @author:     Richard Winters
 * @copyright:  2022 Richard B Winters
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/


// INCLUDES
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/index';


// DEFINES
const store = createStore( rootReducer, applyMiddleware( thunk ) );


// Let's export our redux store:
export default store;

