/*-----------------------------------------------------------------------------
 * @package:    React Project Template
 * @author:     Richard Winters
 * @copyright:  2022 Richard B Winters
 * @license:    Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>
 * @version:    0.1.1
 *---------------------------------------------------------------------------*/


 // Absolutely declare this always.
 'use strict';


// INCLUDES
import gulp from 'gulp';                            // Obviously :)
import del from 'del';                              // Used by our clean process, for the most part -
import bump from 'gulp-bump-version';               // Used for version increment automation


// GLOBALS

const args =                                        // Allows us to accept arguments to our gulpfile
(
    argList =>
    {
        let args = {}, i, option, thisOption, currentOption;

        for( i = 0; i < argList.length; i++ )
        {
            thisOption = argList[i].trim();

            option = thisOption.replace( /^-+/, '' );

            if( option === thisOption )
            {
                // argument value
                if( currentOption )
                {
                    args[currentOption] = option;
                }

                currentOption = null;
            }
            else
            {
                currentOption = option;
                args[currentOption] = true;
            }
        }

        return args;
    }
)( process.argv );


// Check if we supplied arguments specific to version bump processes:
let bumpVersion         = args['bump-version'],
    toVersion           = args['version'],
    byType              = args['type'],
    bumpOptions         = ( bumpVersion ) ? { } : { type: 'patch' },
    projectBumpOptions  = ( bumpVersion ) ? { } : { type: 'patch', key: '"version": "' };

// If version was provided, overwrite the options accordingly:
if( toVersion )
{
    bumpOptions = { version: toVersion };
    projectBumpOptions = { version: toVersion, key: '"version": "' };
    moduleBumpOptions = { version: toVersion, key: 'private projectVersion: string = "' };
}

// If type was provided, overwrite the options accordingly:
if( byType )
{
    bumpOptions = { type: byType };
    projectBumpOptions = { type: byType, key: '"version": "' };
    moduleBumpOptions = { type: byType, key: 'private projectVersion: string = "' };
}


// Bump the version in our projects files:
gulp.task
(
    'bump-file-versions',
    () =>
    {
        // Start with all the files using the typical key, then hit our package.json
        // 'dist/src/**/*.js':
        return gulp.src
        (
            [
                '**/*.ts',
                '**/*.tsx',
                '**/*.js',
                '**/*.scss',
                '!node_modules{,/**}',
                '!test{,/**}',
                '!build/test{,/**}'
            ],
            { base: './' }
        )
        .pipe( bump( bumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Bump the version in our package.json file:
gulp.task
(
    'bump-project-version',
    () =>
    {
        // Target our package.json:
        return gulp.src
        (
            'package.json',
            { base: './' }
        )
        .pipe( bump( projectBumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Bumps the binary's version in our cli.ts source file:
gulp.task
(
    'bump-module-version',
    () =>
    {
        // Target src/cli.ts:
        return gulp.src
        (
            'src/path/to/module.ts',
            { base: './' }
        )
        .pipe( bump( moduleBumpOptions ) )
        .pipe( gulp.dest( '.' ) );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'clean:build',
    ( done ) =>
    {
        let deleteList = ( !args.alt ) ? 'dist' : 'build';
        del.sync
        (
            deleteList,
            { force: true }
        );

        // While typescript compiler can just return its process,
        // for some reason del.sync() needs to trigger async
        // completion (ensure you provide the argument to the
        // anon function so that it's available):
        done();
    }
);


// Runs clean processes:
gulp.task
(
    'clean',
    gulp.series
    (
        'clean:build',
        ( done ) =>
        {
            done();
        }
    )
);


// Cleans the dist directory and bump all file header version strings:
gulp.task
(
    'build-release',
    gulp.series
    (
        'clean:dist',
        'bump-project-version',
        'bump-file-versions',
        ( done ) =>
        {
            done();
        }
    )
);

